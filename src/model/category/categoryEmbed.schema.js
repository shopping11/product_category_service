const mongoose = require('mongoose')

exports.CategoryEmbedSchema = mongoose.Schema({
    id: {
        type: mongoose.SchemaTypes.ObjectId
    },
    name: {
        type: String,
    }
}, {_id: false})