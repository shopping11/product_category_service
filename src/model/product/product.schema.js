const mongoose = require('mongoose')
const { AuthorStampSchema } = require('../../../service-share/model/author-stamp.schema')
const authorStampPlugin = require('../../../service-share/plugin/author-stamp.plugin')
const { globalMethodPlugin } = require('../../../service-share/plugin/global.method')
const { PictureSchema } = require('./picture.schema')
const mongoosePaginate = require('mongoose-paginate-v2')
const { CategoryEmbedSchema } = require('../category/categoryEmbed.schema')
const codeGeneratePlugin = require('../../../service-share/plugin/codeGenerate.plugin')
const { Status } = require('./product.enum')
const { orderBookingDate } = require('./orderBookingDate.schema')
const { ProductPickType } = require('./productPickType.schema')

const ProductSchema = new mongoose.Schema({
    code: {
        type: String
    },
    name: {
        type: String,
        required: true,
        trim: true
    },
    slug: {
        type: String,
        require: true,
        unique: true
    },
    description: {
        type: String,
    },
    category: {
        type: CategoryEmbedSchema,
        default: {}
    },
    price: {
        type: Number
    },
    quantity: {
        type: Number
    },
    bookingDates: {
        type: [ orderBookingDate ],
        default: []
    },
    picture: {
        type: [ PictureSchema ],
        default: [],
        validate: v => v.length > 0 && v.length <= 5
    },
    productPickType: {
        type: ProductPickType,
        default: {}
    },
    statusProduct: {
        type: String,
        enum: [Status.ACTIVE, Status.INACTIVE],
        default: Status.ACTIVE
    },
    createdBy: {
        type: AuthorStampSchema,
        default: {}
    },
    updatedBy: {
        type: AuthorStampSchema,
    }
}, { timestamps: true })

ProductSchema.plugin(globalMethodPlugin)
ProductSchema.plugin(authorStampPlugin)
ProductSchema.plugin(mongoosePaginate)
ProductSchema.plugin(codeGeneratePlugin,'PD')

module.exports = mongoose.model('Product', ProductSchema)