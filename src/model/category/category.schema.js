const mongoose = require('mongoose')
const { AuthorStampSchema } = require('../../../service-share/model/author-stamp.schema')
const authorStampPlugin = require('../../../service-share/plugin/author-stamp.plugin')
const { globalMethodPlugin } = require('../../../service-share/plugin/global.method') 
const { CategoryEmbedSchema } = require('./categoryEmbed.schema')

const CategorySchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        trim: true
    },
    slug: {
        type: String,
        require: true,
        unique: true
    },
    parentCategory: {
        type: CategoryEmbedSchema,
        default: {}
    },
    createdBy: {
        type: AuthorStampSchema,
        default: {}
    },
    updatedBy: {
        type: AuthorStampSchema,
        default: {}
    }

}, { timestamps: true })

CategorySchema.plugin(globalMethodPlugin)
CategorySchema.plugin(authorStampPlugin)


module.exports = mongoose.model('Category', CategorySchema)