const { check, query } = require('express-validator')
const { ValidateExistError, ValidateExist } = require('../../service-share/validator/validateExist.validator')

exports.ValidateProduct = [
    check('name')
        .notEmpty()
        .withMessage('กรุณารุะบุ'),
    check('category.id')
        .notEmpty(),
    check('category.name')
        .notEmpty(),
    check('price')
        .notEmpty()
        .withMessage('กรุณารุะบุ')
        .bail()
        .isNumeric()
        .withMessage('กรุณารุะบุให้ถูกต้อง'),
    check('picture')
        .notEmpty()
        .withMessage('กรุณารุะบุ')
        .bail()
        .custom((value) => {
            if (value.length > 5) {
                throw new Error('รูปเกิน5');
            }
            return true
        }),
    check('quantity')
        .notEmpty()
        .withMessage('กรุณารุะบุ')
        .bail()
        .isNumeric()
        .withMessage('กรุณารุะบุให้ถูกต้อง'),
    check('productPickType')
    .optional()
]

exports.ValidatePatchProduct = [
    check('name')
        .optional(),
    check('category.id')
        .notEmpty(),
    check('category.name')
        .notEmpty(),
    check('price')
        .notEmpty()
        .withMessage('กรุณารุะบุ')
        .bail()
        .isNumeric()
        .withMessage('กรุณารุะบุให้ถูกต้อง'),
    check('picture')
        .notEmpty()
        .withMessage('กรุณารุะบุ')
        .bail()
        .custom((value) => {
            if (value.length > 5) {
                throw new Error('รูปเกิน5');
            }
            return true
        }),
    check('quantity')
        .notEmpty()
        .withMessage('กรุณารุะบุ')
        .bail()
        .isNumeric()
        .withMessage('กรุณารุะบุให้ถูกต้อง'),
]


exports.ValidatePatchProductStatus = [
    check('statusProduct')
    .notEmpty()
    .withMessage('กรุณารุะบุ')
]

exports.ValidateProductQuery = [
    query('limit')
        .optional()
        .default(10),
    query('page')
        .optional()
        .default(1),
    query('orderBy')
        .optional()
        .default('desc'),
    query('search')
        .optional()
        
]

exports.ValidateProductPublicQuery = [
    query('page')
        .optional()
        .default(1),
    query('search')
        .optional()
]