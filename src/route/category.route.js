const express = require('express')
const router = express.Router()
const { CreateCategory, GetCategoryList, UpdateCategory, DeleteCategory, TestError } = require('../controller/category/category.controller')
const { ValidateCategory, ValidatePatchCategory } = require('../validate/categoey.validator')
const { IsRequestValidated } = require('../../service-share/validator/common-validator')
const { requireSignin } = require('../../service-share/middle-ware/token.middle-ware')

router.get('/error', TestError)
router.post('/category', requireSignin, ValidateCategory, IsRequestValidated, CreateCategory)
router.get('/category/front', GetCategoryList)
router.get('/category', requireSignin, GetCategoryList)
router.patch('/category/:category', requireSignin, ValidatePatchCategory, IsRequestValidated, UpdateCategory)
router.delete('/category/:category',requireSignin,  DeleteCategory )


module.exports = router