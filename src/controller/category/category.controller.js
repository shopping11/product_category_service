const { asyncHandler, ErrorHandler, AppResponseError } = require("../../../service-share/middle-ware/response.middle-ware")
const CategoryService = require('../../service/category.service')
const slug = require('slug')
const { APP_BAD_REQUEST } = require("../../../service-share/const/bad-request.const")
const ProductService = require('../../service/product.service')

exports.CreateCategory = asyncHandler(async (req, res) => {
    const { name } = req.body
    const replaceSpaceName = name.split(' ').join('')
    const categoryExist = await CategoryService.FindCategoryNameExist(replaceSpaceName)
    if (categoryExist) {
        throw new AppResponseError(400,APP_BAD_REQUEST.CATEGORY_NAME_DUPLICATE)
    }

    req.body.name = replaceSpaceName
    req.body.slug = slug(replaceSpaceName)
    const category = await CategoryService.CreateCategory(req)

    return category
})

function BuildListParentCategory(category, parentCategory = null) {
    let categories
    let list = []
    if (parentCategory == null) {
        categories = category.filter(cat => cat.parentCategory.id === undefined)
    } else {
        categories = category.filter(cat => cat.parentCategory.id === parentCategory)
    }
    for (let cat of categories) {
        list.push({
            id: cat.id,
            name: cat.name,
            slug: cat.slug,
            parentCategory: cat.parentCategory,
            children: BuildListParentCategory(category,cat.id)
        })
    }

    return list;
}

exports.GetCategoryList = asyncHandler(async (req, res) => {
    const categories = await CategoryService.GetCategoryList()
    const buildCategories = BuildListParentCategory(categories)
    return buildCategories
})

exports.UpdateCategory = asyncHandler(async (req, res) => {
    const { name } = req.body
    if (name) {
        const replaceSpaceName = name.split(' ').join('')
        const categoryExist = await CategoryService.FindCategoryNameExist(replaceSpaceName)
        if (categoryExist) {
            throw new AppResponseError(400, APP_BAD_REQUEST.CATEGORY_NAME_DUPLICATE)
        }
        req.body.name = replaceSpaceName
        req.body.slug = slug(replaceSpaceName)
    }
    const categoryDoc = await CategoryService.FindIdCategoryDoc(req)

    if (categoryDoc.parentCategory.name != req.body.parentCategory.name) {
    const categoryParent = await CategoryService.PatchFindParentCategory(categoryDoc.id)
        if (categoryParent) {
            throw new AppResponseError(400,APP_BAD_REQUEST.CATEGORY_NAME_CHILD_EXIST)
        }
    }
    if (name) { 
        await ProductService.PatchNameFromPatchCategory(req.params.category,name)
    }
    
    return await CategoryService.UpdateCategory(categoryDoc, req)
})

exports.DeleteCategory = asyncHandler(async (req, res) => {
    const category = await CategoryService.FindParentCategory(req)
    if (category) {
        throw new AppResponseError(400,APP_BAD_REQUEST.CATEGORY_NAME_CHILD_EXIST)
    }
    await CategoryService.DeleteCategory(req)

    return 'Delete Success ...!'
})

exports.TestError = asyncHandler(async(req, res) => {
    await CategoryService.TestError(req,res)

    return "done"
})