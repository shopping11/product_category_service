const express = require('express')
const { requireSignin } = require('../../service-share/middle-ware/token.middle-ware')
const { IsRequestValidated } = require('../../service-share/validator/common-validator')
const { CreateProduct, UpdateProduct, GetProduct,
GetProductByCategory, DeleteProduct, UpdateProductStatus, GetBookingDate, PatchBookingDate, GetProductPublic, GetProductByName, TestProvider, GetProductByIdAndGroupBookingDates, PatchBookingFromOrder } = require('../controller/product/product.contrller')
const { DeleteBookingInProduct } = require('../controller/product/productOrder.controller')
const { ValidateProduct, ValidateProductQuery, ValidatePatchProductStatus, ValidatePatchProduct, ValidateProductPublicQuery } = require('../validate/product.validator')
const router = express.Router()

router.get('/product', requireSignin, ValidateProductQuery, IsRequestValidated, GetProduct)
router.get('/product/public', ValidateProductPublicQuery, IsRequestValidated, GetProductPublic)
router.get('/product/:product', GetProductByName)
router.get('/product/group-booking-date/:id', GetProductByIdAndGroupBookingDates)
router.get('/product/public/:category', ValidateProductPublicQuery, GetProductByCategory)
router.patch('/delete/booking-product',requireSignin, DeleteBookingInProduct)
router.post('/product', requireSignin,ValidateProduct, IsRequestValidated , CreateProduct )
router.patch('/product/:product', requireSignin, ValidatePatchProduct, IsRequestValidated, UpdateProduct)
router.patch('/product/status/:product', requireSignin,ValidatePatchProductStatus, IsRequestValidated , UpdateProductStatus )
router.delete('/product/:product', requireSignin, DeleteProduct)
router.get('/product/booking/:product', GetBookingDate)
router.patch('/product/order/booking', requireSignin, PatchBookingFromOrder)
router.patch('/product/order/booking/:product', requireSignin, PatchBookingDate)


module.exports = router