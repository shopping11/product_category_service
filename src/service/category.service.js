const mongoose  = require('mongoose')
const { ErrorHandler } = require('../../service-share/middle-ware/response.middle-ware')
const CategoryModel = require('../model/category/category.schema')

// exports.FindOneCategory = async (categoryId) => {
//     const doc = await CategoryModel.findOne({_id: categoryId})

//     return doc.toObject()
// }

exports.FindCategoryNameExist = async (name) => {
    const doc = await CategoryModel.findOne({name: name})

    return doc
}

exports.FindParentCategory = async (req) => {
    const doc = await CategoryModel.findOne({'parentCategory.id': req.params.category})

    return doc
}

exports.PatchFindParentCategory = async (id) => {
    const doc = await CategoryModel.findOne({'parentCategory.id': id})

    return doc
}

exports.FindIdCategoryDoc = async (req) => {
    const doc = await CategoryModel.findOne({ _id: req.params.category })
    
    return doc
}

exports.CreateCategory = async (req) => {
    const doc = new CategoryModel(req.body)
    doc.setAuthor(req.user)
    await doc.save()

    return doc.toObject()
}

exports.GetCategoryList = async () => {
    const docs = await CategoryModel.find({})

    return docs.map(doc => doc.toObject())
}

exports.UpdateCategory = async (document,req) => {
    // const doc = await CategoryModel.findOneAndUpdate({_id:req.params.category}, req.body, { new: true, _requestUser: req.user })
    const options = {
        _requestUser: req.user
    }

    return document.set(req.body).save(options)
}

exports.DeleteCategory = async (req) => {
    const doc = await CategoryModel.findOneAndDelete({_id: req.params.category})
    
    return doc.toObject()
}

exports.TestError = async () => {
    await CategoryModel.findById({ _id: "616d1b82727c01" }).exec().then((data) => {
        if (!data) {
            throw new ErrorHandler(400,'test erro in service')
        }
    })
    
}