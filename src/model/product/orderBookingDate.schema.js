const mongoose = require('mongoose')

const ProductPickType = new mongoose.Schema({
    mainType: {
        type: String
    },
    subType: {
        type: String
    }
}, { _id: false })

exports.orderBookingDate = new mongoose.Schema({
    id: {
        type: mongoose.SchemaTypes.ObjectId
    },
    code: {
        type: String
    },
    quantity: {
        type: Number
    },
    bookingDates: {
        type: [ Date ] 
    },
    productPickType: {
        type: ProductPickType,
    }
}, {_id: false})