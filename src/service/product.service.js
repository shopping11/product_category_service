const ProductModel = require('../model/product/product.schema')
const mongoose = require("mongoose")
const { ErrorHandler } = require('../../service-share/middle-ware/response.middle-ware')

exports.GetPaginateProduct = async (req) => {
    const query = {
        $or: [
            {name: { $regex: req.query.search, $options: "i" }},
            { 'category.name': { $regex: req.query.search, $options: "i" } },
            {'code': { $regex: req.query.search, $options: "i" }},
        ]
    }

    const options = {
        page: req.query.page,
        limit: req.query.limit,
        sort: { createdAt: req.query.orderBy },
        select: '_id category statusProduct createdBy name price \
        quantity picture productPickType createdAt updatedAt updatedBy code __v'
    }
    
    const {docs, ...filter} = await ProductModel.paginate(query, options)

    return {docs: docs.map(doc => doc.toObject()),filter}
}

exports.GetPaginateProductPublic = async (req) => {
    const query = {
        $or: [
            {name: { $regex: req.query.search, $options: "i" }},
            { 'category.name': { $regex: req.query.search, $options: "i" } },
            { 'code': { $regex: req.query.search, $options: "i" } },
        ],
        statusProduct : 'active' 
    }

    const options = {
        page: req.query.page,
        limit: 20,
        sort: { createdAt: 'desc' },
        select: '_id category statusProduct createdBy name price \
        quantity picture productPickType createdAt updatedAt updatedBy code __v'
    }
    
    const {docs, ...filter} = await ProductModel.paginate(query, options)

    return {docs: docs.map(doc => doc.toObject()),filter}
}

exports.FindIdProductDoc = async (req) => {
    const doc = await ProductModel.findOne({ _id: req.params.product })
    
    return doc
}

exports.FindIdProductDocSelectProductType = async (req) => {
    const doc = await ProductModel.findOne({ _id: req.params.product }).select('productPickType')
    
    return doc
}

exports.GetPaginateProductByCategory = async (req, categoriesList) => {
    const query = {
        'category.name': categoriesList
    }
    const options = {
        page: req.query.page,
        limit: 20
      }
    const { docs, ...filter } = await ProductModel.paginate(query, options)


    return {docs: docs.map(doc => doc.toObject()),filter}
}

exports.FindProductNameExist = async (name) => {
    const doc = ProductModel.findOne({name: name})
    return doc
}

// exports.GetProductByName = async (req) => {
//     const doc = await ProductModel.findOne({ name: req.params.product, statusProduct : 'active' })
//     if (!doc) {
//         throw new ErrorHandler(404, "Not Found")
//     }
    
//     return doc.toObject()
// }

exports.GetProductByName = async (req) => { 
    let dateCheck = new Date();
    dateCheck.setDate(dateCheck.getDate() - 1);

    const doc = await ProductModel.aggregate([
        {
            $match: {
                name: req.params.product,
                statusProduct: 'active'
            }
        },
        {
            $addFields: {
                id: '$_id'
            }
        }, 
        {
            $unset: "_id"
        },
        {
            $addFields: {
                'bookingDates': {
                    $filter: {
                        input: '$bookingDates',
                        as: 'bookingDate',
                        cond: {
                            $gt: [
                                {
                                    $size: {
                                        $filter: {
                                            input: '$$bookingDate.bookingDates',
                                            cond: {
                                                $gte: [
                                                    '$$this',
                                                    // new Date()
                                                    dateCheck
                                                ]
                                            }
                                        }
                                    }
                                },
                                0
                            ]
                        }
                    }
                } 
            }
        }
    ])

    if (!doc[0]) {
        throw new ErrorHandler(404, "Not Found")
    }
    
    return doc[0]
}

exports.GetProductById = async (req) => {
    let dateCheck = new Date();
    dateCheck.setDate(dateCheck.getDate() - 1);

    const doc = await ProductModel.aggregate([
        {
            $match: {
                _id: new mongoose.Types.ObjectId(req.params.id),
                statusProduct: 'active'
            }
        },
        {
            $addFields: {
                id: '$_id'
            }
        }, 
        {
            $unset: "_id"
        },
        {
            $addFields: {
                'bookingDates': {
                    $filter: {
                        input: '$bookingDates',
                        as: 'bookingDate',
                        cond: {
                            $gt: [
                                {
                                    $size: {
                                        $filter: {
                                            input: '$$bookingDate.bookingDates',
                                            cond: {
                                                $gte: [
                                                    '$$this',
                                                    // new Date()
                                                    dateCheck
                                                ]
                                            }
                                        }
                                    }
                                },
                                0
                            ]
                        }
                    }
                } 
            }
        }
    ])

    if (!doc[0]) {
        throw new ErrorHandler(404, "Not Found")
    }
    
    return doc[0]
}

exports.CreateProduct = async (req) => {
    const doc = new ProductModel(req.body)
    doc.setAuthor(req.user)
    await doc.save()

    return doc.toObject()
}

exports.UpdateProduct = async (document, req) => {
    // const doc = await ProductModel.findOneAndUpdate({_id:req.params.product}, req.body, { new: true, _requestUser: req.user })
    
    // return doc.toObject()
    const options = {
        _requestUser: req.user
    }

    return document.set(req.body).save(options)
}

exports.UpdateProductStatus = async (document,req) => {
    const options = {
        _requestUser: req.user
    }

    return document.set(req.body).save(options)
}

exports.DeleteProduct = async(req) => {
    const doc = await ProductModel.findOneAndDelete({_id:req.params.product})
}

exports.PatchNameFromPatchCategory = async (id, name) => { 
    console.log(id,name)
    const doc = await ProductModel.updateMany({ 'category.id': id }, { "$set": { "category.name": name } })
}

exports.GetBookingDate = async (req) => { 
    // const doc = await ProductModel.findOne({ _id: req.params.product })
    let dateCheck = new Date();
    dateCheck.setDate(dateCheck.getDate() - 1);

    const doc = await ProductModel.aggregate([
        {
            $match: {
                '_id': new mongoose.Types.ObjectId(req.params.product)
            }
        },
        {
            $addFields: {
                id: '$_id'
            }
        }, 
        {
            $unset: "_id"
        },
        {
            $addFields: {
                'bookingDates': {
                    $filter: {
                        input: '$bookingDates',
                        as: 'bookingDate',
                        cond: {
                            $gt: [
                                {
                                    $size: {
                                        $filter: {
                                            input: '$$bookingDate.bookingDates',
                                            cond: {
                                                $gte: [
                                                    '$$this',
                                                    // new Date()
                                                    dateCheck
                                                ]
                                            }
                                        }
                                    }
                                },
                                0
                            ]
                        }
                    }
                } 
            }
        }
    ])
    
    // return doc.toObject()
    return doc
}

// exports.GetBookingDate = async (req) => { 
//     // const doc = await ProductModel.findOne({ _id: req.params.product })

//     const doc = await ProductModel.aggregate([
//         {
//             $match: {
//                 '_id': new mongoose.Types.ObjectId(req.params.product)
//             }
//         },
//         {
//             $unwind: "$bookingDates"
//         },
//         {
//             $match: {
//               "bookingDates.bookingDates": {
//                 $gte: new Date()
//               }
//             }
//         },
//         {
//             $group: {
//                 _id: "$_id",
//                 quantity: {
//                     $first: "$quantity"
//                 },
//                 productPickType: {
//                     $first: "$productPickType"
//                 },
//               bookingDates: {
//                 $addToSet: "$bookingDates"
//               }
//             }
//           }
//     ])
    
//     // return doc.toObject()
//     return doc
// }

// exports.PatchBookingDate = async (document,req) => {
//     const options = {
//         _requestUser: req.user
//     }

//     // return document.set({bookingDate:[...document.bookingDate,...req.body.bookingDate]}).save(options)
//     return document.set({bookingDate:[...document.bookingDate,req.body.bookingDate]}).save(options)
// }



exports.PatchBookingDateFindAndUpdate = async (req) => { 
    
    return ProductModel.findOneAndUpdate({ _id: req.params.product }, {
        "$push": {
            bookingDates: req.body.bookingDates
        }
    },{ new: true, _requestUser: req.user })
}

exports.GetBookingDateV2 = async (id) => { 
    // const doc = await ProductModel.findOne({ _id: req.params.product })
    let dateCheck = new Date();
    dateCheck.setDate(dateCheck.getDate() - 1);

    const doc = await ProductModel.aggregate([
        {
            $match: {
                '_id': new mongoose.Types.ObjectId(id)
            }
        },
        {
            $addFields: {
                id: '$_id'
            }
        }, 
        {
            $unset: "_id"
        },
        {
            $addFields: {
                'bookingDates': {
                    $filter: {
                        input: '$bookingDates',
                        as: 'bookingDate',
                        cond: {
                            $gt: [
                                {
                                    $size: {
                                        $filter: {
                                            input: '$$bookingDate.bookingDates',
                                            cond: {
                                                $gte: [
                                                    '$$this',
                                                    // new Date()
                                                    dateCheck
                                                ]
                                            }
                                        }
                                    }
                                },
                                0
                            ]
                        }
                    }
                } 
            }
        }
    ])
    
    // return doc.toObject()
    return doc
}

exports.PatchBookingDateFindAndUpdateV2 = async (req, id, payload, session) => { 
    
    return ProductModel.findOneAndUpdate({ _id: id }, {
        "$push": {
            bookingDates: payload
        }
    },{ new: true, _requestUser: req.user, session: session })
}

exports.DeleteBookingInProduct = async (req, id, orderId, session) => { 
    const doc = ProductModel.findOneAndUpdate({ _id: id }, {
        "$pull": {
            bookingDates: {
                id: orderId
            }
        }
    }, { new: true, _requestUser: req.user, session: session })

    return doc
}