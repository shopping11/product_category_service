// const mongoose = require('mongoose')

// const ProductType = new mongoose.Schema({
//     type: {
//         type: String
//     }
// }, { _id: false })

// exports.ProductPickType = new mongoose.Schema({
//     nameType: {
//         type: String
//     },
//     productType: {
//         type: [ProductType]
//     }

// }, { _id: false })



const mongoose = require('mongoose')
const { PictureSchema } = require('./picture.schema')

const ProductSubType = new mongoose.Schema({
    nameType: {
        type: String
    },
    quantity: {
        type: Number
    },
    price: {
        type: Number
    }
}, { _id: false })

const SubType = new mongoose.Schema({
    nameSubType: {
        type: String
    },
    productSubTypes: {
        type: [ProductSubType]
    }
}, { _id: false })

const ProductType = new mongoose.Schema({
    nameType: {
        type: String
    },
    subType: {
        type: SubType
    },
    quantity: {
        type: Number
    },
    price: {
        type: Number
    },
    picture: {
        type: PictureSchema,
        default: {}
    }
}, { _id: false })

exports.ProductPickType = new mongoose.Schema({
    nameType: {
        type: String
    },
    productTypes: {
        type: [ProductType],
        default: []
    }

}, { _id: false })