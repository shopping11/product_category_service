const express = require('express')
const app = express()
const { responseHandle } = require('./service-share/middle-ware/response.middle-ware')
const mongooseConnection = require("./src/mongoose/mongoose.connection")
const morgan = require('morgan')
const cors = require('cors')
require('dotenv').config()
const CategoryRoute = require('./src/route/category.route')
const ProductRoute = require('./src/route/product.route')

//connect mongoose
mongooseConnection()

//middleware
app.use(express.json())
app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(morgan('dev'))

// route
app.use('/products', CategoryRoute, ProductRoute)

//middleware hook response
app.use((data, req, res, next) => {
  responseHandle(data,req,res,next)
})

app.listen(process.env.PORT, () => {
  console.log("server is running on port", process.env.PORT)
});