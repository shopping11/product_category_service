const { check } = require('express-validator')
const { ValidateExistError } = require('../../service-share/validator/validateExist.validator')

exports.ValidateCategory = [
    check('name')
        .notEmpty()
        .withMessage('กรุณารุะบุ'),
    check('parentCategory.id')
        .optional(),
    check('parentCategory.name')
        .optional()
]

exports.ValidatePatchCategory = [
    check('parentCategory.id')
        .optional(),
    check('parentCategory.name')
        .optional()
]
