const { asyncHandler, ErrorHandler, AppResponseError } = require("../../../service-share/middle-ware/response.middle-ware")
const slug = require('slug')
const ProductService = require('../../service/product.service')
const CategoryService = require('../../service/category.service')
const { APP_BAD_REQUEST } = require("../../../service-share/const/bad-request.const")
const { DateTime } = require('luxon')
const withTransaction = require('../../mongoose/mongoose.transection')


exports.GetProduct = asyncHandler(async (req, res) => {

    return ProductService.GetPaginateProduct(req)
})

exports.GetProductPublic = asyncHandler(async (req, res) => {

    return ProductService.GetPaginateProductPublic(req)
})

exports.GetProductByName = asyncHandler(async (req, res) => { 
    const product = await ProductService.GetProductByName(req)
    const groupBookingDates = BuildDisableDate(product)
    const { bookingDates, ...newProduct } = product
    
    return {
        ...newProduct,
        disableDate: groupBookingDates
    }

    // return ProductService.GetProductByName(req)
})

exports.GetProductByIdAndGroupBookingDates = asyncHandler(async (req, res) => { 
    const product = await ProductService.GetProductById(req)

    const groupBookingDates = BuildGroupBooking(product)
    const { bookingDates, ...newProduct } = product
    
    return {
        ...newProduct,
        groupBoookingDates: groupBookingDates
    }
})

exports.CreateProduct = asyncHandler(async (req, res) => {
    const { name } = req.body
    
    const productExist = await ProductService.FindProductNameExist(name)
    if (productExist) {
        throw new AppResponseError(400,APP_BAD_REQUEST.PRODUCT_NAME_DUPLICATE)
    }

    req.body.slug = slug(name)
    
    return ProductService.CreateProduct(req)
})

exports.UpdateProduct = asyncHandler(async (req, res) => {
    const { name } = req.body

    if (name) { 
        const productExist = await ProductService.FindProductNameExist(name)
        if (productExist) {
            throw new AppResponseError(400,APP_BAD_REQUEST.PRODUCT_NAME_DUPLICATE)
        }
    
        req.body.slug = slug(name)
    
    }

    const productDoc = await ProductService.FindIdProductDoc(req)

    return ProductService.UpdateProduct(productDoc, req)
})

exports.UpdateProductStatus = asyncHandler(async (req, res) => {
    const productDoc = await ProductService.FindIdProductDoc(req)
    
    return await ProductService.UpdateProductStatus(productDoc,req)
})

exports.DeleteProduct = asyncHandler( async (req, res) => {
    await ProductService.DeleteProduct(req)
    
    return 'Delete Success ...!'
})

function BuildListCategory(categories,parentCategory, list = []) {
    let categoriesList
    list.push(parentCategory)
    categoriesList = categories.filter(category => category.parentCategory.name == parentCategory)
    
    categoriesList.forEach((categoryList) => {
        
    BuildListCategory(categories,categoryList.name,list)

    }) 

    return list
}

exports.GetProductByCategory = asyncHandler(async (req, res) => {
    const categories = await CategoryService.GetCategoryList()
    const categoriesList = BuildListCategory(categories, req.params.category)

    return ProductService.GetPaginateProductByCategory(req,categoriesList)
})

exports.GetBookingDate = asyncHandler(async (req, res) => { 
    const product = await ProductService.GetBookingDate(req)

    if (product[0].bookingDates.length > 0) {
        const disbleDate = BuildDisableDate(product[0])

        return {
            // disbleDate: disbleDate.filter(date => date.qty >= product.quantity)
            disbleDate: disbleDate
        }
    }

    return {disbleDate: []}

    // return product
})

const BuildPickType = (productPickType) => { 
    const buildQtyPickType = []
    productPickType.productTypes.map((productType) => { 
        if (productType.subType) {
            // productType.subTypes.map((subType) => {
                productType.subType.productSubTypes.map((productSubType) => {
                    buildQtyPickType.push({
                        mainType: productType.nameType,
                        subType: productSubType.nameType,
                        quantity: productSubType.quantity
                    })
                 })
            // })
        } else { 
            buildQtyPickType.push({
                mainType: productType.nameType,
                quantity: productType.quantity
            })
        }

    })
    
    return buildQtyPickType
}

const checkDisbleDate = (groupDate, buildQtyPickType) => { 
    // let disbleDate
    // if (buildQtyPickType.length > 0) { 
    //     disbleDate = groupDate.filter((date) => buildQtyPickType.some((pickType) =>
    //         date.productPickType?.mainType + '-' + date.productPickType?.subType === pickType?.mainType + '-' + pickType?.subType &&
    //         date.quantity >= pickType.quantity
    //     ))
    //     return disbleDate
    // } 

    // disbleDate = groupDate.filter((date) => date.quantity >= buildQtyPickType)

    // return disbleDate
    let disbleDate
    if (buildQtyPickType.length > 0) { 
        console.log(buildQtyPickType)
        disbleDate = groupDate.map((date) => { 
            const findTypeIndex = buildQtyPickType.findIndex((pickType) => date.productPickType?.mainType + '-' + date.productPickType?.subType === pickType?.mainType + '-' + pickType?.subType)
            // date.productPickType?.mainType + '-' + date.productPickType?.subType , buildQtyPickType[findType]?.mainType + '-' + buildQtyPickType[findType]?.subType,
            if (date.quantity >= buildQtyPickType[findTypeIndex].quantity) { 
                return {...date,disbleStatus: true, avaliableBook:0}
            }

            return {...date,disbleStatus: false, avaliableBook: buildQtyPickType[findTypeIndex].quantity-date.quantity }
        })
        return disbleDate
    } 

    // disbleDate = groupDate.filter((date) => date.quantity >= buildQtyPickType)
    disbleDate = groupDate.map((date) => { 
        if (date.quantity >= buildQtyPickType) { 
            return {...date,disbleStatus: true, avaliableBook:0}
        }

        return {...date,disbleStatus: false, avaliableBook: buildQtyPickType-date.quantity }
    })

    return disbleDate

}

const BuildGroupBooking = (product) => {
    const groupDate = [];
    product.bookingDates.reduce((acc, curr) => {
        curr.bookingDates.forEach((date) => {
            if (!acc[date + '-' + curr.productPickType?.mainType + '-' + curr.productPickType?.subType]){
           acc[date + '-' + curr.productPickType?.mainType + '-' + curr.productPickType?.subType] = { date: date, quantity: 0, productPickType: curr.productPickType };
           groupDate.push(acc[date + '-' + curr.productPickType?.mainType + '-' + curr.productPickType?.subType])
        }

         acc[date + '-' + curr.productPickType?.mainType + '-' + curr.productPickType?.subType].quantity += curr.quantity
    }) 
        return acc
    }, {})

    return groupDate
}

const BuildDisableDate = (product) => { 
    const groupDate = [];
    product.bookingDates.reduce((acc, curr) => {
        curr.bookingDates.forEach((date) => {
            if (!acc[date + '-' + curr.productPickType?.mainType + '-' + curr.productPickType?.subType]){
           acc[date + '-' + curr.productPickType?.mainType + '-' + curr.productPickType?.subType] = { date: date, quantity: 0, productPickType: curr.productPickType };
           groupDate.push(acc[date + '-' + curr.productPickType?.mainType + '-' + curr.productPickType?.subType])
        }

         acc[date + '-' + curr.productPickType?.mainType + '-' + curr.productPickType?.subType].quantity += curr.quantity
    }) 
        return acc
    }, {})


    let buildQtyPickType
    if (product.productPickType?.productTypes?.length > 0 ) {
        buildQtyPickType = BuildPickType(product.productPickType)
    } else { 
        buildQtyPickType = product.quantity
    }
    
    
    // return groupDate
    return checkDisbleDate(groupDate,buildQtyPickType)
}

const CheckPickType = (product, bookingDates) => {                                     
    if (product?.productPickType?.productTypes.length === 0 && !bookingDates?.productPickType?.mainType) { 
        return
    }
    if (product?.productPickType?.productTypes.length > 0) { 
      const productPickTypes  = BuildPickType(product.productPickType)
        if (productPickTypes.some((productPickType) => productPickType?.mainType + '-' + productPickType?.subType ===
            bookingDates.productPickType?.mainType + '-' + bookingDates.productPickType?.subType)) { 

            return
        }
    }
    
    throw new ErrorHandler(400, 'Error need to reload page for update data')
}

exports.PatchBookingDate = asyncHandler(async (req, res) => { 
    const { bookingDates } = req.body
    const productDoc = await ProductService.FindIdProductDocSelectProductType(req)
    CheckPickType(productDoc, bookingDates)

    const product = await ProductService.GetBookingDate(req)
    

        const groupBookingDates = GroupBookingDate(product[0], bookingDates)
        bookingDates.bookingDates.map(date => {
            if (DateTime.fromISO(new Date().toISOString()).setLocale('th').toFormat('yyyy/MM/dd') > date) {
                throw new ErrorHandler(400, "Error cant order pass date")
            }
            groupBookingDates.map(groupBookingDate => {
                if (DateTime.fromISO(groupBookingDate.date.toISOString()).setLocale('th').toFormat('yyyy/MM/dd') === date &&
                    groupBookingDate.productPickType?.mainType + '-' + groupBookingDate.productPickType?.subType === bookingDates.productPickType?.mainType + '-' + bookingDates.productPickType?.subType) {
                    throw new ErrorHandler(400, 'not enough item for booking')
      
                }
            })
        })

    return await ProductService.PatchBookingDateFindAndUpdate(req)
})

exports.PatchBookingFromOrder = asyncHandler(async (req, res) => { 
    const { orderItems, ...order } = req.body

    return withTransaction(async (ClientSession) => { 
        const PromiseItems =  orderItems.map(async(orderItem) => { 
        
        const product = await ProductService.GetBookingDateV2(orderItem.id)
            
        CheckPickType(product[0], orderItem)
        const groupBookingDates = GroupBookingDate(product[0], orderItem)
            
        orderItem.bookingDates.map(date => {
            if (DateTime.fromISO(new Date().toISOString()).setLocale('th').toFormat('yyyy/MM/dd') > date) {
                throw new ErrorHandler(400, "Error cant order pass date")
            }
            groupBookingDates.map(groupBookingDate => {
                if (DateTime.fromISO(groupBookingDate.date.toISOString()).setLocale('th').toFormat('yyyy/MM/dd') === date &&
                    groupBookingDate.productPickType?.mainType + '-' + groupBookingDate.productPickType?.subType === orderItem.productPickType?.mainType + '-' + orderItem.productPickType?.subType) {
                    throw new ErrorHandler(400, 'not enough item for booking')
      
                }
            })
        })
    
        const buildOrderItem = {
                id: order.id,
                code: order.code,
                quantity: orderItem.quantity,
                productPickType: orderItem.productPickType,
                bookingDates: orderItem.bookingDates
        }
    
        await ProductService.PatchBookingDateFindAndUpdateV2(req, orderItem.id, buildOrderItem, ClientSession)
       
        })
        await Promise.all(PromiseItems)
        
        return 'success'
    })
     
})

exports.TestProvider = asyncHandler(async (req, res) => { 

    return "done"

})

const checkDisbleDateUpdate = (groupDate, buildQtyPickType) => { 
    let disbleDate
    if (buildQtyPickType.length > 0) { 
        disbleDate = groupDate.filter((date) => buildQtyPickType.some((pickType) =>
            date.productPickType?.mainType + '-' + date.productPickType?.subType === pickType?.mainType + '-' + pickType?.subType &&
            date.quantity > pickType.quantity
        ))
        return disbleDate
    } 

    disbleDate = groupDate.filter((date) => date.quantity > buildQtyPickType)

    return disbleDate

}



const GroupBookingDate = (product, bookingDates) => { 
    const groupDates = [];
    product.bookingDates.reduce((acc, curr) => {
        curr.bookingDates.forEach((date) => {
            if (!acc[date + '-' + curr.productPickType?.mainType + '-' + curr.productPickType?.subType]){
           acc[date + '-' + curr.productPickType?.mainType + '-' + curr.productPickType?.subType] = { date: date, quantity: 0, productPickType: curr.productPickType };
           groupDates.push(acc[date + '-' + curr.productPickType?.mainType + '-' + curr.productPickType?.subType])
        }

         acc[date + '-' + curr.productPickType?.mainType + '-' + curr.productPickType?.subType].quantity += curr.quantity
    }) 
        return acc
    }, {})


    bookingDates.bookingDates.forEach((bookingDate) => { 
        if (groupDates.some((groupDate) => new Date(bookingDate) + '-' + bookingDates.productPickType?.mainType + '-' + bookingDates.productPickType?.subType ===
        groupDate.date + '-' + groupDate.productPickType?.mainType + '-' +
            groupDate.productPickType?.subType)) { 
                
            const groupDateIndex = groupDates.findIndex((groupDate) => new Date(bookingDate) + '-' + bookingDates.productPickType?.mainType + '-' + bookingDates.productPickType?.subType ===
            groupDate.date + '-' + groupDate.productPickType?.mainType + '-' +
            groupDate.productPickType?.subType)
            
            groupDates[groupDateIndex] = {...groupDates[groupDateIndex],quantity: groupDates[groupDateIndex].quantity + bookingDates.quantity  }

        return
        }
        groupDates.push({ date: new Date(bookingDate), quantity: bookingDates.quantity, productPickType: bookingDates.productPickType})
    })
    
    
    let buildQtyPickType
    if (product.productPickType?.productTypes?.length > 0 ) {
        buildQtyPickType = BuildPickType(product.productPickType)
    } else { 
        buildQtyPickType = product.quantity
    }
    
    
    return checkDisbleDateUpdate(groupDates,buildQtyPickType)

}