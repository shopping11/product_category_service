const withTransaction = require('../../mongoose/mongoose.transection')
const ProductService = require('../../service/product.service')
const { asyncHandler, ErrorHandler, AppResponseError } = require("../../../service-share/middle-ware/response.middle-ware")

exports.DeleteBookingInProduct = asyncHandler(async (req, res) => { 
    const { id, orderItems } = req.body
    return withTransaction(async (ClientSession) => { 
        const PromiseProducts = orderItems.map(async (orderItem) => { 
            await ProductService.DeleteBookingInProduct(req, orderItem.id, id, ClientSession)
        })
        await Promise.all(PromiseProducts)

        return 'success'
    })
})